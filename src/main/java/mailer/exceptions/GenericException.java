package mailer.exceptions;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class GenericException extends Exception {

    private static final long serialVersionUID = -5781799913689135289L;
    private GenericError genericError;
    private String reason;

    public GenericException(GenericError genericError, String reason) {
        super(reason);
        this.genericError = genericError;
        this.reason = reason;
    }

    public int getHttpErrorCode() {
        return 400;
    }
}
