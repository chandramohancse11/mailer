package mailer.Services.sql;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class QueryBuilderService {
  public QueryBuilderService() {
  }

  public static String prepareMultiInsertQueryForContentAssoc(String query, JsonObject data) {
    StringBuffer outputQuery = new StringBuffer(query);
    int contentId = data.getInteger("contentId").intValue();
    JsonArray ids = data.getJsonArray("ids");
    String strToAppend = " (%d , %d)";

    for(int i = 0; i < ids.size(); ++i) {
      outputQuery.append(String.format(strToAppend, contentId, ids.getInteger(i)) + " , ");
    }

    return outputQuery.toString().replaceAll(", $", "") + ";";
  }
}

