package mailer.Services.sql;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;

public class QueryUtil extends BaseUtil {
  private static JDBCClient jdbcClient = null;
  public static final int ERROR_CODE = 100;
  public static int JDBC_ERROR_CODE = 100;
  public static int DUPLICATE_ENTITY_ERROR_CODE = 101;
  public static String UNIQUE_KEY_EXCEPTION = "com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException";


  public QueryUtil() {
  }

  public static void querySql(JsonObject jsonObj, Message<Object> message, String readMode) throws Exception {
    boolean withParams = jsonObj.containsKey("params");
    boolean isQuery = jsonObj.getString("type").equalsIgnoreCase("query");
    String sqlString = jsonObj.getString("statement");
    if (withParams) {
      JsonArray params = jsonObj.getJsonArray("params");
      if (isQuery) {
        processQueryWithParams(sqlString, params, message, readMode);
      } else {
        processUpdateWithParams(sqlString, params, message, readMode);
      }
    } else if (isQuery) {
      processQuery(sqlString, message, readMode);
    } else {
      processUpdate(sqlString, message, readMode);
    }

  }

  private static void processQuery(String sqlString, Message<Object> message, String readMode) throws Exception {
    jdbcClient = JDBCClientService.getJdbcClient(readMode);
    jdbcClient.getConnection((conn) -> {
      if (conn.failed()) {
        message.fail(100, "JDBC connection failed");
      } else {
        SQLConnection _sqlCon = (SQLConnection)conn.result();
        if (_sqlCon == null) {
          message.fail(100, "JDBC connection failed");
        } else {
          _sqlCon.query(sqlString, (query) -> {
            if (query.failed()) {
              message.fail(100, sqlString + " failed " + query.cause() + " " + query.cause().getMessage());
            } else {
              JsonObject obj = ((ResultSet)query.result()).toJson();
              if (obj != null) {
                message.reply(obj.toString());
              } else {
                message.fail(100, "Empty query result");
              }
            }

            _sqlCon.close();
          });
        }
      }

    });
  }

  private static void processUpdate(String sqlString, Message<Object> message, String readMode) throws Exception {
    jdbcClient = JDBCClientService.getJdbcClient(readMode);
    jdbcClient.getConnection((conn) -> {
      if (conn.failed()) {
        message.fail(100, "JDBC connection failed");
      } else {
        SQLConnection _sqlCon = (SQLConnection)conn.result();
        if (_sqlCon == null) {
          message.fail(100, "JDBC connection failed");
        } else {
          _sqlCon.update(sqlString, (query) -> {
            if (query.failed()) {
              message.fail(getErrorCode(query.cause()), sqlString + " failed " + query.cause() + " " + query.cause().getMessage());
            } else {
              JsonObject obj = ((UpdateResult)query.result()).toJson();
              if (obj != null) {
                message.reply(obj.toString());
              } else {
                message.fail(100, "Empty query result");
              }
            }

            _sqlCon.close();
          });
        }
      }

    });
  }

  public static int getErrorCode(Throwable throwable) {
    logger.info("Inside getErrorCode function ");
    String exceptionClass = throwable.getClass().getName();
    int errorCode = JDBC_ERROR_CODE;
    if (UNIQUE_KEY_EXCEPTION.equals(exceptionClass)) {
      errorCode = DUPLICATE_ENTITY_ERROR_CODE;
    }

    return errorCode;
  }

  private static void processQueryWithParams(String sqlString, JsonArray params, Message<Object> message, String readMode) throws Exception {
    jdbcClient = JDBCClientService.getJdbcClient(readMode);
    jdbcClient.getConnection((conn) -> {
      if (conn.failed()) {
        message.fail(100, "JDBC connection failed");
      } else {
        SQLConnection _sqlCon = (SQLConnection)conn.result();
        if (_sqlCon == null) {
          message.fail(100, "JDBC connection failed");
        } else {
          _sqlCon.queryWithParams(sqlString, params, (query) -> {
            if (query.failed()) {
              message.fail(100, sqlString + " failed " + query.cause() + " " + query.cause().getMessage());
            } else {
              JsonObject obj = ((ResultSet)query.result()).toJson();
              if (obj != null) {
                message.reply(obj.toString());
              } else {
                message.fail(100, "Empty query result");
              }
            }

            _sqlCon.close();
          });
        }
      }

    });
  }

  private static void processUpdateWithParams(String sqlString, JsonArray params, Message<Object> message, String readMode) throws Exception {
    jdbcClient = JDBCClientService.getJdbcClient(readMode);
    jdbcClient.getConnection((conn) -> {
      if (conn.failed()) {
        message.fail(100, "JDBC connection failed");
      } else {
        SQLConnection _sqlCon = (SQLConnection)conn.result();
        if (_sqlCon == null) {
          message.fail(100, "JDBC connection failed");
        } else {
          _sqlCon.updateWithParams(sqlString, params, (query) -> {
            if (query.failed()) {
              message.fail(getErrorCode(query.cause()), sqlString + " failed " + query.cause() + " " + query.cause().getMessage());
            } else {
              JsonObject obj = ((UpdateResult)query.result()).toJson();
              if (obj != null) {
                message.reply(obj.toString());
              } else {
                message.fail(100, "Empty query result");
              }
            }

            _sqlCon.close();
          });
        }
      }

    });
  }

  public static void fireQueryWithParams(String query, JsonArray params) {
    JDBCClientService.getJdbcClient().getConnection((connection) -> {
      if (connection.failed()) {
        logger.error(connection.cause());
      } else {
        SQLConnection sqlConnection = (SQLConnection)connection.result();
        if (null == sqlConnection) {
          logger.error("SQLConnection is null");
          return;
        }

        sqlConnection.queryWithParams(query, params, (response) -> {
          if (response.failed()) {
            logger.error(response.cause());
          } else {
            logger.info(((ResultSet)response.result()).toJson().toString());
          }

          sqlConnection.close();
        });
      }

    });
  }
}

