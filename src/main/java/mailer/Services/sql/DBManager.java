package mailer.Services.sql;

import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public final class DBManager extends BaseUtil {
  private static final Logger logger = LoggerFactory.getLogger(DBManager.class);
  public static int JDBC_ERROR_CODE = 100;
  public static int DUPLICATE_ENTITY_ERROR_CODE = 101;
  public static String UNIQUE_KEY_EXCEPTION = "com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException";



  private DBManager() {
  }

  public static void insertInTxn(JsonObject messageJson, Message<Object> message) {
    logger.info("Received message " + messageJson);
    JDBCClient jdbcClient = JDBCClientService.getJdbcClient();
    Future<Void> queryExecutionFuture = Future.future();
    jdbcClient.getConnection((connection) -> {
      if (connection.failed()) {
        logger.info("Connection failed due to" + connection.cause().getMessage());
        message.fail(JDBC_ERROR_CODE, "JDBC connection failed");
      } else {
        SQLConnection sqlConnection = (SQLConnection)connection.result();
        executeQueriesInTxn(messageJson, message, sqlConnection, queryExecutionFuture);
      }

    });
    queryExecutionFuture.setHandler((handler) -> {
      if (handler.succeeded()) {
        logger.info("Succeeded in SQL Fetcher Insertions");
        message.reply("success");
      } else {
        logger.info("Sending failure message to caller application");
        message.fail(JDBC_ERROR_CODE, handler.cause().getMessage());
      }

    });
  }

  private static void executeQueriesInTxn(JsonObject messageJson, Message<Object> message, SQLConnection sqlConnection, Future<Void> queryExecutionFuture) {
    AtomicBoolean success = new AtomicBoolean(true);
    Future<Void> startTxnFuture = Future.future();
    Future<Void> endTxnFuture = Future.future();
    JsonArray queries = messageJson.getJsonArray("queries");
    AtomicInteger counter = new AtomicInteger(queries.size());
    StringBuffer messageBuffer = new StringBuffer();
    startTx(sqlConnection, startTxnFuture);
    startTxnFuture.setHandler((handler) -> {
      if (handler.failed()) {
        logger.info("Failure in starting Txn :" + handler.cause().getMessage());
        queryExecutionFuture.fail(handler.cause());
        message.fail(JDBC_ERROR_CODE, "Unable to Start Txn");
        sqlConnection.close();
      } else {
        logger.info("Txn Started!!!");
        String query = null;

        for(int i = 0; i < queries.size(); ++i) {
          query = queries.getString(i);
          logger.info("Running query : " + query);
          sqlConnection.update(query, (result) -> {
            counter.getAndDecrement();
            if (result.failed()) {
              logger.info("Failure in executing query : " + result.cause().getMessage());
              messageBuffer.append(" " + result.cause().getMessage());
              success.set(false);
            }

            if (counter.get() == 0 && success.get()) {
              logger.info("Marking queryExecutionFuture  as complete");
              endTx(sqlConnection, endTxnFuture);
            } else if (counter.get() == 0 && !success.get()) {
              logger.info("Marking queryExecutionFuture as failure");
              queryExecutionFuture.fail(message.toString());
              sqlConnection.close();
            }

          });
        }
      }

    });
    endTxnFuture.setHandler((handler) -> {
      if (handler.failed()) {
        logger.info("Failure in Committing Changes due to : " + handler.cause().getMessage());
        sqlConnection.close();
        queryExecutionFuture.fail(handler.cause());
      } else {
        logger.info("End Transaction is successful , Committed DB Changes");
        sqlConnection.close();
        queryExecutionFuture.complete();
      }

    });
  }




  public static void insertInTxnWithParams(JsonObject messageJson, Message<Object> message) {
    logger.info("Received message " + messageJson);
    JDBCClient jdbcClient = JDBCClientService.getJdbcClient();
    Future<Void> queryExecutionFuture = Future.future();
    jdbcClient.getConnection((connection) -> {
      if (connection.failed()) {
        logger.info("Connection failed due to" + connection.cause().getMessage());
        message.fail(JDBC_ERROR_CODE, "JDBC connection failed");
      } else {
        SQLConnection sqlConnection = (SQLConnection)connection.result();
        executeQueriesInTxnWithParams(messageJson, message, sqlConnection, queryExecutionFuture);
      }

    });
    queryExecutionFuture.setHandler((handler) -> {
      if (handler.succeeded()) {
        logger.info("Succeeded in SQL Fetcher Insertions");
        message.reply("success");
      } else {
        logger.info("Sending failure message to caller application");
        message.fail(JDBC_ERROR_CODE, handler.cause().getMessage());
      }

    });
  }

  private static void executeQueriesInTxnWithParams(JsonObject messageJson, Message<Object> message, SQLConnection sqlConnection, Future<Void> queryExecutionFuture) {
    AtomicBoolean success = new AtomicBoolean(true);
    Future<Void> startTxnFuture = Future.future();
    Future<Void> endTxnFuture = Future.future();
    JsonArray queries = messageJson.getJsonArray("queries");
    AtomicInteger counter = new AtomicInteger(queries.size());
    StringBuffer messageBuffer = new StringBuffer();
    startTx(sqlConnection, startTxnFuture);
    startTxnFuture.setHandler((handler) -> {
      if (handler.failed()) {
        logger.info("Failure in starting Txn :" + handler.cause().getMessage());
        queryExecutionFuture.fail(handler.cause());
        message.fail(JDBC_ERROR_CODE, "Unable to Start Txn");
        sqlConnection.close();
      } else {
        logger.info("Txn Started!!!");
        String query = null;
        JsonArray params = null;

        for(int i = 0; i < queries.size(); ++i) {
          query = queries.getJsonObject(i).getString("query");
          params = queries.getJsonObject(i).getJsonArray("params");
          logger.info("Running query : " + query + " with params : " + params);
          sqlConnection.queryWithParams(query, params, (result) -> {
            counter.getAndDecrement();
            if (result.failed()) {
              logger.info("Failure in executing query : " + result.cause().getMessage());
              messageBuffer.append(" " + result.cause().getMessage());
              success.set(false);
            }

            if (counter.get() == 0 && success.get()) {
              logger.info("Marking queryExecutionFuture  as complete");
              endTx(sqlConnection, endTxnFuture);
            } else if (counter.get() == 0 && !success.get()) {
              logger.info("Marking queryExecutionFuture as failure");
              queryExecutionFuture.fail(message.toString());
              sqlConnection.close();
            }

          });
        }
      }

    });
    endTxnFuture.setHandler((handler) -> {
      if (handler.failed()) {
        logger.info("Failure in Committing Changes due to : " + handler.cause().getMessage());
        sqlConnection.close();
        queryExecutionFuture.fail(handler.cause());
      } else {
        logger.info("End Transaction is successful , Committed DB Changes");
        sqlConnection.close();
        queryExecutionFuture.complete();
      }

    });
  }
}

