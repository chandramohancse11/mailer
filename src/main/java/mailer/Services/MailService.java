package mailer.Services;

import java.util.ArrayList;
import java.util.List;
import mailer.factories.MailServicesFactory;
import mailer.interfaces.MailServices;

public class MailService {

  public static MailServicesFactory mailServicesFactory = new MailServicesFactory();


  public static void sendMail(String mail_services, String metadata) {

    List<MailServices> mailServicesList = mailServicesFactory.getMailServices(mail_services);
    for (MailServices ms : mailServicesList) {
      ms.send(metadata);
    }
  }


}
