package mailer.interfaces.impl;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.MailMessage;
import io.vertx.rxjava.ext.mail.MailClient;
import mailer.Entities.EmailRequest;
import mailer.config.ConfigurationManager;
import mailer.interfaces.MailServices;
import mailer.utils.LogFactory;
import mailer.utils.Utils;
import mailer.verticles.MailVerticle;
import mailer.verticles.MainVerticle;

public class EmailSender implements MailServices{
  private static MailClient mailClient;
  private static final Logger logger = LogFactory.getLogger(MailVerticle.class);


  public static EmailSender instance = null;

  @Override
  public void send(String Metadata) {
    EmailRequest emailRequest = Utils.getClassObject(Metadata, EmailRequest.class);
    MailMessage email = new MailMessage()
        .setFrom(emailRequest.getFrom())
        .setTo(emailRequest.getTo())
        .setBounceAddress(emailRequest.getBounceAddress())
        .setSubject(emailRequest.getSubject())
        .setHtml(emailRequest.getMessage());

    mailClient.rxSendMail(email).subscribe(mailResult -> {
      logger.info("Email sent successfully");
    }, err -> {
      logger.error("Unable to send email." + emailRequest);

      //TODO: add retry logic
    });
  }

  private EmailSender()
  {

  }

  // static method to create instance of Singleton class
  public static EmailSender getInstance()
  {
    if (instance == null) {
      instance = new EmailSender();
      JsonObject mailConfig = ConfigurationManager.getConfig().getJsonObject("MAIL_SERVER_CONFIG");
      if(mailConfig == null){
        logger.error("Exception while starting mail verticle");
      }
      mailClient = MailClient.createShared(MainVerticle.vertxObj, new MailConfig(mailConfig));
    }

    return instance;
  }

}
