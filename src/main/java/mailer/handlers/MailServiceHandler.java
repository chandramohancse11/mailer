package mailer.handlers;

import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.core.http.HttpServerRequest;
import io.vertx.rxjava.ext.web.RoutingContext;
import java.util.UUID;
import mailer.Services.MailService;
import mailer.utils.LogFactory;
import mailer.utils.Utils;


public class MailServiceHandler {

  private static Logger logger = LogFactory.getLogger(MailServiceHandler.class);

  public static void sendMail(RoutingContext routingContext) {
    Utils.validateAPIKey(routingContext);
    HttpServerRequest request = routingContext.request();
    String requestId = UUID.randomUUID().toString();
    logger.info("Send Mail call with requestId request: " + requestId
        + ": Headers" + Utils.getJson(routingContext.request().headers().getDelegate().entries())
        + ". \t Query params: "+Utils.getJson(routingContext.request().params().getDelegate().entries())
        + ". \t Body: "+Utils.getJson(routingContext.getBodyAsJson()));
    JsonObject apiResponse = new JsonObject();
    MailService.sendMail(routingContext.request().params().get("mail_services"),routingContext.getBodyAsJson().encodePrettily());
      apiResponse.put("data", apiResponse);
      Utils.sendSuccess(apiResponse,request.response(),201);

  }


}
