package mailer.factories;

import java.util.ArrayList;
import java.util.List;
import mailer.interfaces.MailServices;
import mailer.interfaces.impl.EmailSender;

public class MailServicesFactory {


  public List<MailServices> getMailServices(String type) {
    String[] tokens = type.split(",");
    List<MailServices> sc = new ArrayList<>();
    for (String str : tokens) {
      sc.add(getMailService(str));

    }
    return sc;
  }


  public MailServices getMailService(String type) {
    if (type == null) {
      return null;
    }
    if (type.equalsIgnoreCase("EmailSender")) {
      return  EmailSender.getInstance();

    } else if (type.equalsIgnoreCase("SMSSender")) {
      return EmailSender.getInstance();

    } else if (type.equalsIgnoreCase("GCM")) {
      return EmailSender.getInstance();
    }

    return null;
  }


}


