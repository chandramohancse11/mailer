package mailer.dao.impl;


import mailer.utils.Constants;
import mailer.dao.BaseDAO;
import mailer.exceptions.GenericError;
import mailer.exceptions.GenericException;
import mailer.utils.LogFactory;
import io.vertx.core.Future;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.eventbus.EventBus;
import rx.Observable;



public class BaseDAOImpl implements BaseDAO {

	private static final Logger logger = LogFactory.getLogger(BaseDAOImpl.class);
	protected static final EventBus eventBus = Vertx.currentContext().owner().eventBus(); //Vertx.currentContext().owner().eventBus();
	

	@Override
	public Observable<JsonArray> findAll(String query, JsonArray params, String dbReadMode) {
		return Observable.create(subscriber -> {
			logger.info("params = "+params.toString());
			logger.info("query = "+query);
			JsonObject searchQueryJson = new JsonObject();
			searchQueryJson.put("statement", query );
			searchQueryJson.put("type", "query");
			searchQueryJson.put("params", params);
			searchQueryJson.put("db_read_mode",dbReadMode);
			
			Vertx.currentContext().owner().eventBus().send("bit.tracker.sqlfetcher", searchQueryJson, response  -> {
				if (response.succeeded()) {
					JsonObject sqlResponseJson = new JsonObject(response.result().body().toString());
					JsonArray responseArray = sqlResponseJson.getJsonArray("rows");
		    	 	logger.info("response = "+ responseArray);
		    	 	subscriber.onNext(responseArray);
					subscriber.onCompleted();
				}else {
					logger.info("response failed= " + response.cause());
					subscriber.onError(response.cause());
					subscriber.onCompleted();
				}
	        });
		});
	}


	@Override
	public Observable<String> update(String query, JsonArray params) {
		return Observable.create(subscriber -> {
			logger.info("params = "+params.toString());
			logger.info("query = "+query);
			JsonObject updateQuery = new JsonObject();
			updateQuery.put("statement", query );
			updateQuery.put("type", "update");
			updateQuery.put("params", params);
			
			Vertx.currentContext().owner().eventBus().send("bit.tracker.sqlfetcher", updateQuery, response  -> {
				if (response.succeeded()) {
					String resBodyStr = (String) response.result().body();
					JsonObject responseJson = new JsonObject(resBodyStr);
					logger.info("response from sqlfetcher is = " + responseJson);
					Integer rowsUpdated = responseJson.getInteger("updated");
					if(rowsUpdated <= 0) {
						subscriber.onError(new GenericException(GenericError.ENTITY_NOT_FOUND, "Entity not found"));
					} else {
						subscriber.onNext(rowsUpdated.toString());
					}
					subscriber.onCompleted();
				}else {
					logger.info("response failed= "+response.cause());
					ReplyException exception = (ReplyException) response.cause();
					if (exception.failureCode() == Constants.DUPLICATE_ENTITY_ERROR_CODE)
						subscriber.onError(new GenericException(GenericError.DUPLICATE_ENTITY, "Entity already exists"));
					else
						subscriber.onError(response.cause());
				}
	        });
		});
	}



	
}
