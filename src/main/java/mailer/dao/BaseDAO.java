package mailer.dao;

import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.List;
import rx.Observable;

public interface BaseDAO {

	Observable<String> update(String query, JsonArray params);
	Observable<JsonArray> findAll(String query, JsonArray params, String dbReadMode);

}