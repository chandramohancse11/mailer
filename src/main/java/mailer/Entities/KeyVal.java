package mailer.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KeyVal<T extends Comparable<T>, R extends Comparable<R>> implements Comparable<KeyVal<T, R>> {
    private T key;
    private R value;

    @Override
    public int compareTo(KeyVal<T, R> o) {
        return key.compareTo(o.key);
    }
}
