package mailer.Entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmailRequest {
  private String to;
  private String from;
  private String message;
  private String bounceAddress;
  private String subject;
}
