package mailer.utils;

/**
 * Created by chandra.rai on 10/04/18.
 */
public final class Constants {

  private Constants() {

  }
  public static final int DUPLICATE_ENTITY_ERROR_CODE = 101;

  public final static String INVOICE_INSERT = "INSERT INTO `invoice` "
      + "(`order_id`,`total_price`,`tax`)"
      + " VALUES "
      + "(?,?,?) ";

  public final static String GET_INVOICE = "select * from invoice where order_id= ?  ";


}
