package mailer.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.http.HttpServerResponse;
import io.vertx.rxjava.ext.web.RoutingContext;
import java.util.List;
import java.util.concurrent.TimeUnit;
import mailer.config.ConfigurationManager;
import mailer.exceptions.GenericError;
import mailer.exceptions.GenericException;
import rx.Observable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import rx.functions.Func1;

/**
 * Created by chandra.rai on 20/06/18.
 */
public final class Utils {
    private static final Logger logger = LogFactory.getLogger(Utils.class);
    private static ObjectMapper mapper;
    //private static String validateTokensEventAddress = ConfigurationManager.getConfig().getString("validate_tokens_event_bus_address");

    public static ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            mapper.registerModule(new JodaModule());
        }
        return mapper;
    }

    public static String encode(byte[] bytes) {
        try {
            return new String(Base64.getEncoder().encode(bytes), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Can't perform base64 encoding", e);
        }
    }

    public static String encode(String data) {
        try {
            return new String(Base64.getEncoder().encode(data.getBytes()), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Can't perform base64 encoding", e);
        }
    }

    public static String decode(String string) {
        try {
            return new String(Base64.getDecoder().decode(string), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Can't perform base64 decoding", e);
        }
    }

    public static void sendSuccess(JsonObject apiResponse, HttpServerResponse response) {
        response.putHeader("content-type", "application/json");
        JsonObject rsp = new JsonObject().put("status", "SUCCESS");
        rsp.put("response", apiResponse);
        response.end(rsp.toString());
    }

    public static void sendSuccess(JsonObject apiResponse, HttpServerResponse response, Integer statusCode) {
        response.putHeader("content-type", "application/json");
        JsonObject rsp = new JsonObject().put("status", "SUCCESS");
        rsp.put("response", apiResponse);
        response.end(rsp.toString());
        response.setStatusCode(statusCode);
    }

    public static void sendSuccess(String JsonAPIResponse, HttpServerResponse response) {
        JsonObject responseObj = new JsonObject(JsonAPIResponse);
        sendSuccess(responseObj, response);
    }

    public static void sendSuccessResponse(String responseString, HttpServerResponse response) {
        response.putHeader("content-type", "application/json");
        JsonObject rsp = new JsonObject().put("status", "SUCCESS");
        rsp.put("response", responseString);
        response.end(rsp.toString());
    }

    public static void sendFailure(String reason, HttpServerResponse response) {
        response.putHeader("content-type", "application/json");
        JsonObject rsp = new JsonObject().put("status", "FAILURE");
        rsp.put("reason", reason);
        response.end(rsp.toString());
    }

    public static void sendFailure(HttpServerResponse httpServerResponse, String reason, int statusCode) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("status", "FAILURE");
        jsonObject.put("reason", reason);
        httpServerResponse.putHeader("content-type", "application/json; charset=utf-8");
        httpServerResponse.putHeader("charset", "UTF-8");
        httpServerResponse.setStatusCode(statusCode);
        httpServerResponse.end(jsonObject.toString());

    }

    public static void validateAPIKey(RoutingContext routingContext){
       String apiKey = routingContext.request().getHeader("api-key");
       if (apiKey == null) {
            routingContext.fail(new GenericException(GenericError.MISSING_PARAMETERS, "API key in header is mandatory"));
            return;
       }
       if(!apiKey.equals(ConfigurationManager.getConfig().getString("api-key"))){
            routingContext.fail(new GenericException(GenericError.WRONG_API_KEY, "Wrong API key"));
       }
    }

    public static <T> T getClassObject(ObjectMapper mapper, String data, Class<T> tClass) {
        T object = null;
        try {
            object = mapper.readValue(data, tClass);
        } catch (IOException e) {
            logger.error("Error in getting class instance from data: " + data + "\nError Message: " + e.getMessage());
        }
        return object;
    }

    public static <T> T getClassObject(String data, Class<T> tClass) {
        T object = null;
        try {
            object = mapper.readValue(data, tClass);
        } catch (IOException e) {
            logger.error("Error in getting class instance from data: " + data + "\nError Message: " + e.getMessage());
        }
        return object;
    }

    public static <T> T getClassObject(String data, TypeReference<?> typeReference) {
        T object = null;
        try {
            object = getMapper().readValue(data, typeReference);
        } catch (IOException e) {
            throw new RuntimeException("Error in getting class instance from data: " + data + "\nError Message: " + e.getMessage());
        }
        return object;
    }

    public static <T> String getJson(T object, TypeReference<?> typeReference) {
        String jsonString = "{}";
        try {
            jsonString = getMapper().writerFor(typeReference).writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("Error in getting string value of object, error: " + e.getMessage());
        }
        return jsonString;
    }

    public static <T> String getJson(ObjectMapper mapper, T object) {
        String jsonString = "{}";
        try {
            jsonString = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("Error in getting string value of object, error: " + e.getMessage());
        }
        return jsonString;
    }

    public static <T> String getJson(T object) {
        String jsonString = "{}";
        try {
            jsonString = getMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("Error in getting string value of object, error: " + e.getMessage());
        }
        return jsonString;
    }

    public static <T> String getJson(ObjectMapper mapper, T object, TypeReference<?> typeReference) {
        String jsonString = "{}";
        try {
            jsonString = mapper.writerFor(typeReference).writeValueAsString(object);
        } catch (JsonProcessingException e) {
            logger.error("Error in getting string value of object, error: " + e.getMessage());
        }
        return jsonString;
    }

    public static boolean isNullOrEmpty(String input) {
        return null == input || input.isEmpty();
    }

    public static void undeployAndCloseVerticle(Vertx vertx, String deploymentId) {
        vertx.rxUndeploy(deploymentId).subscribe(
                success -> {
                    logger.info("Undeployed successfully");
                    vertx.close();
                }
        );

    }


    public static boolean validateJsonResponse(JsonObject response, String[] fields){
        for (String field : fields) {
            if (response.getString(field) == null) {
                return false;
            }
        }
        return true;
    }

//    public static void validateTokensAndRefresh(Map<String, AuthToken> tokens, String userId, String imei) {
//        JsonObject eventJson = new JsonObject().put("tokens", Utils.getJson(tokens, new TypeReference<Map<String, AuthToken>>() {
//        }))
//                                               .put("userId",userId)
//                                               .put("imei",imei);
//        MainVerticle.vertxObj.eventBus().send(validateTokensEventAddress, eventJson);
//    }



    public static boolean isValidImei(String imei) {
        return !Utils.isNullOrEmpty(imei)
            && imei.length()==15
            && Utils.isNumeric(imei);
    }

    public static boolean isNumeric(String input) {
        return input.matches("\\d+");
    }



    public static Func1<Observable<? extends Throwable>, Observable<?>> exponentialBackoff(int maxRetryCount, long delay, TimeUnit unit) {
        return errors -> errors
            .zipWith(Observable.range(1, maxRetryCount), (error, retryCount) -> retryCount)
            .flatMap(retryCount -> Observable.timer((long) Math.pow(delay, retryCount), unit));
    }


    public static boolean isNullValues(String[] fields){
        for (String field : fields) {
            if (Utils.isNullOrEmpty(field)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isInValidImei(String imei) {
        return Utils.isNullOrEmpty(imei)
            || imei.length()!=15
            || !Utils.isNumeric(imei);
    }

    public static boolean isNullOrEmpty(Object input) {
        return null == input;
    }

    public static <T> boolean isNullOrEmpty(List<T> list){
        return null==list || list.size()<=0;
    }

    public static <T> boolean isNullOrEmpty(JsonObject jsonObject){
        return null==jsonObject || jsonObject.size()<=0;
    }

    public static <T> boolean isNotNullOrNonEmpty(JsonObject jsonObject){
        return null!=jsonObject && jsonObject.size() > 0;
    }

    public static boolean isNotNullOrNonEmpty(Object input) {
        return null != input;
    }

    public static <T> boolean isNotNullOrNonEmpty(List<T> list){
        return null != list && list.size() > 0;
    }

    public static boolean isNotNullOrNonEmpty(String input) {
        return null != input && input.length() >0 ;
    }

}
