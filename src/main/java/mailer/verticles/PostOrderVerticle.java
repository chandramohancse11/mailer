package mailer.verticles;

import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.AbstractVerticle;
import java.util.HashMap;
import java.util.Map;
import io.vertx.core.logging.Logger;

import io.vertx.kafka.client.producer.RecordMetadata;
import io.vertx.rxjava.ext.mail.MailClient;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.kafka.client.consumer.KafkaConsumer;
import io.vertx.rxjava.kafka.client.producer.KafkaProducer;
import io.vertx.rxjava.kafka.client.producer.KafkaProducerRecord;
import mailer.Entities.OrderEvent;
import mailer.Services.InvoiceService;
import mailer.Services.MailService;
import mailer.utils.LogFactory;
import mailer.utils.Utils;
import rx.Observable;


public class PostOrderVerticle extends AbstractVerticle {
  private static final Logger logger = LogFactory.getLogger(MailVerticle.class);
  private KafkaConsumer<String, String> ordersQueueConsumer = null;
  private MailClient mailClient;
  private static String ordersKafkaTopicName;
  private static String emailsKafkaTopicName;
  public static String MAIL_SERVER_CONFIG = "mailServerConfig";
  public static String EMAIL_QUEUE_CONSUMER = "emailQueueConsumer";
  public static KafkaProducer<String, String> MailServiceproducer = null;

  @Override
  public void start() {

    initOrderEventRequestKafkaConsumer();
    initMailRequestKafkaProducer();
    startOrderEventRequestKafkaConsumer();

  }

  private void initMailRequestKafkaProducer() {
    JsonObject config = config();
    Map<String, String> props = new HashMap<>();
    logger.info("[Mail Verticle] Initialize Kafka Producer Config with configuration .........." + props);
    MailServiceproducer = KafkaProducer.create(vertx, props, String.class, String.class);
  }

  private void startOrderEventRequestKafkaConsumer() {
    ordersQueueConsumer.handler(kafkaConsumerRecord -> {
      OrderEvent orderMailRequest = Utils.getClassObject(kafkaConsumerRecord.value(), OrderEvent.class);
      MailService.sendMail("sms", orderMailRequest.getMetadata().encodePrettily() );
      Observable<String> observable = InvoiceService.generateInvoice(orderMailRequest.getMetadata());
      observable.subscribe(response->{
        orderMailRequest.getMetadata().put("invoice",response);
        MailService.sendMail("email", orderMailRequest.getMetadata().encodePrettily() );
      },err -> {
        logger.error("Invoice service generation failed ");
        err.printStackTrace();
        pushMailRequests(orderMailRequest.getMetadata().encodePrettily());

      });





    });
    ordersQueueConsumer.subscribe(emailsKafkaTopicName, handle -> {
      if (handle.succeeded()) {
        logger.info("Kafka Consumer subscribed to topic [" + ordersKafkaTopicName + "]");
      } else {
        logger.error("Kafka Consumer could not subscribe to topic [" + ordersKafkaTopicName + "]  Error Cause is "
            + handle.cause().getMessage());
      }
    });
  }

  private void initOrderEventRequestKafkaConsumer() {
    JsonObject config = config();
    Map<String, String> props = new HashMap<>();
    ordersKafkaTopicName = config.getJsonObject(EMAIL_QUEUE_CONSUMER).getString("topic.name");
    ordersQueueConsumer = KafkaConsumer.create(vertx, props);
  }

  public static void pushMailRequests(String emailRequest){
    if(MailServiceproducer != null) {
      KafkaProducerRecord<String, String> record = KafkaProducerRecord.create(emailsKafkaTopicName, emailRequest);
      MailServiceproducer.write(record, done -> {
        if (done.succeeded()) {
          RecordMetadata recordMetadata = done.result();
          logger.info("Message ::" + emailRequest + " written on topic=" + recordMetadata
              .getTopic() +
              ", partition=" + recordMetadata.getPartition() +
              ", offset=" + recordMetadata.getOffset());
        } else {
          Map<String, String> errorParams = new HashMap<>();
          errorParams.put("reason", "notdone");
          logger.error(
              "Exception in publishing to Kafka :: " + emailRequest + "::" + done
                  .cause());
        }
      }).exceptionHandler(ex -> {
        Map<String, String> errorParams = new HashMap<>();
        errorParams.put("reason", "exception in sending to kafka");
        logger.error("Exception in publishing to Kafka :: " + emailRequest + "::" + ex
            .getMessage());
      });
    } else {
      logger.error("Kafka produces for Mail Service is not enabled.");
    }
  }

  }






