package mailer.verticles;

import mailer.Services.sql.BaseUtil;
import mailer.Services.sql.QueryUtil;
import mailer.Services.sql.DBManager;
import mailer.Services.sql.JDBCClientService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

public class SQLFetcher extends AbstractVerticle {
  private Logger logger;

  public SQLFetcher() {
  }

  public void start(Future<Void> startFuture) throws Exception {
    super.start(startFuture);
    this.initLogger();
    BaseUtil.init(this.logger, this.vertx);
    //JDBCClientService.setJdbcClientSlave(this.createJDBCConnectionSlave());
    JDBCClientService.setJdbcClient(this.createJDBCConnection());
    this.fireSampleQuery();
    this.vertx.eventBus().consumer("bit.tracker.sqlfetcher", (message) -> {
      this.logger.info("Got message  :" + message.body().toString());

      try {
        JsonObject jSql = (JsonObject)message.body();
        String readMode = jSql.getString("db_read_mode");
        this.logger.info("db read mode is :");
        QueryUtil.querySql(jSql, message, readMode);
      } catch (Exception var4) {
        message.fail(100, "Oops!!Something was not right while executing the query " + var4);
      }

    });
    this.vertx.eventBus().consumer("bit.tracker.transactional", (message) -> {
      try {
        this.logger.info("in SQLFetcher -- message :" + message.body());
        JsonObject messageJson = (JsonObject)message.body();
        if (messageJson.containsKey("queries")) {
          DBManager.insertInTxnWithParams(messageJson, message);
        } else {
          message.fail(100, "Missing mandatory parameter queries");
        }
      } catch (Exception var3) {
        var3.printStackTrace();
        message.fail(100, "Oops!!Something was not right while executing the query " + var3);
      }

    });
  }

  public JDBCClient createJDBCConnection() {
    String dbConnUrl = this.config().getString("sql_db_conn_url");
    String dbUser = this.config().getString("sql_db_user");
    String dbPasswd = this.config().getString("sql_db_passwd");
    int maxJdbcPoolSize = this.config().getInteger("max_jdbc_poolsize").intValue();
    JDBCClient jdbcClient = JDBCClient.createNonShared(this.vertx, (new JsonObject()).put("url", dbConnUrl).put("user", dbUser).put("password", dbPasswd).put("driver_class", "com.mysql.jdbc.Driver").put("max_pool_size", maxJdbcPoolSize));
    return jdbcClient;
  }

  public JDBCClient createJDBCConnectionSlave() {
    String dbConnUrlSlave = this.config().getString("sql_db_conn_url_slave");
    String dbUserSlave = this.config().getString("sql_db_user_slave");
    String dbPasswdSlave = this.config().getString("sql_db_passwd_slave");
    if (!BaseUtil.isNullOrEmpty(dbPasswdSlave) && !BaseUtil.isNullOrEmpty(dbUserSlave) && !BaseUtil.isNullOrEmpty(dbConnUrlSlave)) {
      int maxJdbcPoolSize = this.config().getInteger("max_jdbc_poolsize").intValue();
      JDBCClient jdbcClient = JDBCClient.createNonShared(this.vertx, (new JsonObject()).put("url", dbConnUrlSlave).put("user", dbUserSlave).put("password", dbPasswdSlave).put("driver_class", "com.mysql.jdbc.Driver").put("max_pool_size", maxJdbcPoolSize));
      return jdbcClient;
    } else {
      return null;
    }
  }

  private void fireSampleQuery() {
    String query = "SELECT 1";
    QueryUtil.fireQueryWithParams(query, new JsonArray());
  }

  public void stop(Future<Void> stopFuture) throws Exception {
    super.stop(stopFuture);
  }

  private void initLogger() throws SecurityException, IOException {
    try {
      InputStream inputStream = SQLFetcher.class.getResourceAsStream("/logging.properties");
      LogManager.getLogManager().readConfiguration(inputStream);
      this.logger = LoggerFactory.getLogger(SQLFetcher.class);
      this.logger.info("Logger initialized");
    } catch (Exception var2) {
      var2.printStackTrace();
    }

  }
}


